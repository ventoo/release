<?php

namespace Phoenix\ReleaseUtil\Tests;

use Phoenix\ReleaseUtil\QueueManager\Command\CommandExecutor;
use Phoenix\ReleaseUtil\QueueManager\Command\CommandInterface;

class CommandExecutorTest extends \PHPUnit\Framework\TestCase
{
    public function testExecute()
    {
        /** @var CommandInterface&\PHPUnit_Framework_MockObject_MockObject $command */
        $command = $this->createMock(CommandInterface::class);
        $command
            ->expects($this->once())
            ->method('execute')
            ->with();

        $executor = new CommandExecutor();
        $executor->execute($command);
    }
}
