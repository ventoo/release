<?php

namespace Phoenix\ReleaseUtil\Tests;

use Phoenix\DataCollector;
use Phoenix\DataCollector\Components\DataCollectorState;
use Phoenix\QueueManager\Components\EQueueManager;
use Phoenix\ReleaseUtil\QueueManager\Command\BindQueueCommand;
use Phoenix\ReleaseUtil\QueueManager\Command\CreateExchangeCommand;
use Phoenix\ReleaseUtil\QueueManager\Command\CreateQueueCommand;
use Phoenix\ReleaseUtil\QueueManager\Command\DeleteExchangeCommand;
use Phoenix\ReleaseUtil\QueueManager\Command\DeleteQueueCommand;
use Phoenix\ReleaseUtil\QueueManager\Command\UnbindQueueCommand;
use Phoenix\ReleaseUtil\QueueManager\DataCollectorCommandProvider;
use Phoenix\ReleaseUtil\QueueManager\ReleaseSnapshotManager;
use Phoenix\ReleaseUtil\QueueManager\Snapshot\Snapshot;

class DataCollectorCommandProviderTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var DataCollector&\PHPUnit_Framework_MockObject_MockObject
     */
    protected $collector;

    /**
     * @var ReleaseSnapshotManager&\PHPUnit_Framework_MockObject_MockObject
     */
    protected $manager;

    protected function setUp(): void
    {
        $manager = $this->createMock(EQueueManager::class);
        $manager->client_key = '8a2e208f0a90449dfffa9e717b3e7d54';
        $this->collector = $this->getMockBuilder(DataCollector::class)
            ->setMethods(['getQueueManager'])
            ->getMock();
        $this->collector->method('getQueueManager')
            ->willReturn($manager);
        $this->manager = $this->createMock(ReleaseSnapshotManager::class);
    }

    public function addConsumerDataProvider()
    {
        return [
            $this->newConsumerDataProvider(),
            $this->lowerConsumerPriorityDataProvider()
        ];
    }

    /**
     * @dataProvider addConsumerDataProvider
     */
    public function testAddConsumer($stateA, $stateB)
    {
        $this->manager->method('getOriginator')
            ->willReturn($this->collector->restore(new Snapshot($stateA)));
        $provider = new DataCollectorCommandProvider($this->collector->restore(new Snapshot($stateB)), $this->manager);
        $commands = $provider->getAll();

        $this->assertCount(1, $commands);
        $this->assertInstanceOf(CreateQueueCommand::class, $commands->first());
    }

    public function delConsumerDataProvider()
    {
        return [
            $this->unsetConsumerDataProvider(),
            $this->increaseConsumerPriorityDataProvider()
        ];
    }

    /**
     * @dataProvider delConsumerDataProvider
     */
    public function testDelConsumer($stateA, $stateB)
    {
        $this->manager->method('getOriginator')
            ->willReturn($this->collector->restore(new Snapshot($stateA)));
        $provider = new DataCollectorCommandProvider($this->collector->restore(new Snapshot($stateB)), $this->manager);
        $commands = $provider->getAll();

        $this->assertCount(1, $commands);
        $this->assertInstanceOf(DeleteQueueCommand::class, $commands->first());
    }

    public function addExtensionDataProvider()
    {
        return [
            $this->newExtensionDataProvider(),
            $this->lowerExtensionPriorityDataProvider()
        ];
    }

    /**
     * @dataProvider addExtensionDataProvider
     */
    public function testAddExtension($stateA, $stateB)
    {
        $this->manager->method('getOriginator')
            ->willReturn($this->collector->restore(new Snapshot($stateA)));
        $provider = new DataCollectorCommandProvider($this->collector->restore(new Snapshot($stateB)), $this->manager);
        $commands = $provider->getAll();

        $this->assertCount(2, $commands);
        $this->assertInstanceOf(CreateExchangeCommand::class, $commands->first());
        $this->assertInstanceOf(BindQueueCommand::class, $commands->last());
    }

    public function delExtensionDataProvider()
    {
        return [
            $this->unsetExtensionDataProvider(),
            $this->increaseExtensionPriorityDataProvider()
        ];
    }

    /**
     * @dataProvider delExtensionDataProvider
     */
    public function testDelExtension($stateA, $stateB)
    {
        $this->manager->method('getOriginator')
            ->willReturn($this->collector->restore(new Snapshot($stateA)));
        $provider = new DataCollectorCommandProvider($this->collector->restore(new Snapshot($stateB)), $this->manager);
        $commands = $provider->getAll();

        $this->assertCount(2, $commands);
        $this->assertInstanceOf(DeleteExchangeCommand::class, $commands->first());
        $this->assertInstanceOf(UnbindQueueCommand::class, $commands->last());
    }

    public function collectorDecreasingPriorityDataProvider()
    {
        $configA = [
            'consumerA' => [
                'queueManager' => 'testQueueManager',
                'extensions' => [
                    'onEventA' => ['entities' => ['default']]
                ]
            ],
            'consumerB' => [
                'priority' => DataCollector::PRIORITY_LOW,
                'queueManager' => 'testQueueManager',
                'extensions' => [
                    'onEventA' => ['entities' => ['default']]
                ]
            ]
        ];

        return [
            [
                new DataCollectorState($configA, [], '', DataCollector::PRIORITY_URGENT, 'testQueueManager', true),
                new DataCollectorState($configA, [], '', DataCollector::PRIORITY_LOW, 'testQueueManager', true)
            ]
        ];
    }

    /**
     * @dataProvider collectorDecreasingPriorityDataProvider
     */
    public function testCollectorDecreasingPriority($stateA, $stateB)
    {
        $this->manager->method('getOriginator')
            ->willReturn($this->collector->restore(new Snapshot($stateA)));
        $provider = new DataCollectorCommandProvider($this->collector->restore(new Snapshot($stateB)), $this->manager);
        $commands = $provider->getAll();

        $this->assertCount(1, $commands);
        $this->assertInstanceOf(CreateQueueCommand::class, $commands->first());
    }

    public function collectorIncreasingPriorityDataProvider()
    {
        $config = [
            'consumerA' => [
                'queueManager' => 'testQueueManager',
                'extensions' => [
                    'onEventA' => ['entities' => ['default']]
                ]
            ],
            'consumerB' => [
                'priority' => DataCollector::PRIORITY_LOW,
                'queueManager' => 'testQueueManager',
                'extensions' => [
                    'onEventA' => ['entities' => ['default']]
                ]
            ]
        ];

        return [
            [
                new DataCollectorState($config, [], '', DataCollector::PRIORITY_LOW, 'testQueueManager', true),
                new DataCollectorState($config, [], '', DataCollector::PRIORITY_URGENT, 'testQueueManager', true)
            ]
        ];
    }

    /**
     * @dataProvider collectorIncreasingPriorityDataProvider
     */
    public function testCollectorIncreasingPriority($stateA, $stateB)
    {
        $this->manager->method('getOriginator')
            ->willReturn($this->collector->restore(new Snapshot($stateA)));
        $provider = new DataCollectorCommandProvider($this->collector->restore(new Snapshot($stateB)), $this->manager);
        $commands = $provider->getAll();

        $this->assertCount(1, $commands);
        $this->assertInstanceOf(DeleteQueueCommand::class, $commands->first());
    }

    public function collectorDisabledConsumersDataProvider()
    {
        $config = [
            'consumerA' => [
                'queueManager' => 'testQueueManager',
                'extensions' => [
                    'onEventA' => ['entities' => ['default']]
                ]
            ],
            'consumerB' => [
                'queueManager' => 'testQueueManager',
                'extensions' => [
                    'onEventA' => ['entities' => ['default']]
                ]
            ]
        ];
        $consumersA = ['consumerB' => 'consumerB'];
        $consumersB = ['consumerA' => 'consumerA'];

        return [
            [
                new DataCollectorState($config, $consumersA, '', DataCollector::PRIORITY_LOW, 'testQueueManager', true),
                new DataCollectorState($config, $consumersB, '', DataCollector::PRIORITY_LOW, 'testQueueManager', true)
            ]
        ];
    }

    /**
     * @dataProvider collectorDisabledConsumersDataProvider
     */
    public function testCollectorDisabledConsumers($stateA, $stateB)
    {
        $this->manager->method('getOriginator')
            ->willReturn($this->collector->restore(new Snapshot($stateA)));
        $provider = new DataCollectorCommandProvider($this->collector->restore(new Snapshot($stateB)), $this->manager);
        $commands = $provider->getAll();

        $this->assertCount(2, $commands);
        $this->assertInstanceOf(CreateQueueCommand::class, $commands->first());
        $this->assertInstanceOf(DeleteQueueCommand::class, $commands->last());
    }

    private function newConsumerDataProvider()
    {
        $configA = [
            'consumerA' => [
                'queueManager' => 'testQueueManager',
                'extensions' => [
                    'onEventA' => ['entities' => ['default']]
                ]
            ]
        ];
        $configB = $configA + [
            'consumerB' => [
                'queueManager' => 'testQueueManager',
                'extensions' => [
                    'onEventA' => ['entities' => ['default']]
                ]
            ]
        ];

        return [
            new DataCollectorState($configA, [], '', DataCollector::PRIORITY_LOW, 'testQueueManager', true),
            new DataCollectorState($configB, [], '', DataCollector::PRIORITY_LOW, 'testQueueManager', true)
        ];
    }

    private function lowerConsumerPriorityDataProvider()
    {
        $configA = [
            'consumerA' => [
                'priority' => DataCollector::PRIORITY_URGENT,
                'queueManager' => 'testQueueManager',
                'extensions' => [
                    'onEventA' => ['entities' => ['default']]
                ]
            ],
            'consumerB' => [
                'queueManager' => 'testQueueManager',
                'extensions' => [
                    'onEventA' => ['entities' => ['default']]
                ]
            ]
        ];
        $configB = $configA;
        $configB['consumerA']['priority'] = DataCollector::PRIORITY_LOW;

        return [
            new DataCollectorState($configA, [], '', DataCollector::PRIORITY_LOW, 'testQueueManager', true),
            new DataCollectorState($configB, [], '', DataCollector::PRIORITY_LOW, 'testQueueManager', true)
        ];
    }

    private function unsetConsumerDataProvider()
    {
        $configA = [
            'consumerA' => [
                'queueManager' => 'testQueueManager',
                'extensions' => [
                    'onEventA' => ['entities' => ['default']]
                ]
            ],
            'consumerB' => [
                'queueManager' => 'testQueueManager',
                'extensions' => [
                    'onEventA' => ['entities' => ['default']]
                ]
            ]
        ];
        $configB = $configA;
        unset($configB['consumerA']);

        return [
            new DataCollectorState($configA, [], '', DataCollector::PRIORITY_LOW, 'testQueueManager', true),
            new DataCollectorState($configB, [], '', DataCollector::PRIORITY_LOW, 'testQueueManager', true)
        ];
    }

    private function increaseConsumerPriorityDataProvider()
    {
        $configA = [
            'consumerA' => [
                'priority' => DataCollector::PRIORITY_LOW,
                'queueManager' => 'testQueueManager',
                'extensions' => [
                    'onEventA' => ['entities' => ['default']]
                ]
            ],
            'consumerB' => [
                'queueManager' => 'testQueueManager',
                'extensions' => [
                    'onEventA' => ['entities' => ['default']]
                ]
            ]
        ];
        $configB = $configA;
        $configB['consumerA']['priority'] = DataCollector::PRIORITY_URGENT;

        return [
            new DataCollectorState($configA, [], '', DataCollector::PRIORITY_LOW, 'testQueueManager', true),
            new DataCollectorState($configB, [], '', DataCollector::PRIORITY_LOW, 'testQueueManager', true)
        ];
    }

    private function newExtensionDataProvider()
    {
        $configA = [
            'consumerA' => [
                'queueManager' => 'testQueueManager',
                'extensions' => [
                    'onEventA' => ['entities' => ['default']]
                ]
            ]
        ];
        $configB = $configA;
        $configB['consumerA']['extensions'] = $configB['consumerA']['extensions'] + [
            'onEventB' => ['entities' => ['default']]
        ];

        return [
            new DataCollectorState($configA, [], '', DataCollector::PRIORITY_LOW, 'testQueueManager', true),
            new DataCollectorState($configB, [], '', DataCollector::PRIORITY_LOW, 'testQueueManager', true)
        ];
    }

    private function lowerExtensionPriorityDataProvider()
    {
        $configA = [
            'consumerA' => [
                'queueManager' => 'testQueueManager',
                'extensions' => [
                    'onEventA' => ['entities' => ['default'], 'priority' => DataCollector::PRIORITY_URGENT],
                    'onEventB' => ['entities' => ['default']]
                ]
            ]
        ];
        $configB = $configA;
        $configB['consumerA']['extensions']['onEventA']['priority'] = DataCollector::PRIORITY_LOW;

        return [
            new DataCollectorState($configA, [], '', DataCollector::PRIORITY_LOW, 'testQueueManager', true),
            new DataCollectorState($configB, [], '', DataCollector::PRIORITY_LOW, 'testQueueManager', true)
        ];
    }

    private function unsetExtensionDataProvider()
    {
        $configA = [
            'consumerA' => [
                'queueManager' => 'testQueueManager',
                'extensions' => [
                    'onEventA' => ['entities' => ['default']],
                    'onEventB' => ['entities' => ['default']]
                ]
            ]
        ];
        $configB = $configA;
        unset($configB['consumerA']['extensions']['onEventA']);

        return [
            new DataCollectorState($configA, [], '', DataCollector::PRIORITY_LOW, 'testQueueManager', true),
            new DataCollectorState($configB, [], '', DataCollector::PRIORITY_LOW, 'testQueueManager', true)
        ];
    }

    private function increaseExtensionPriorityDataProvider()
    {
        $configA = [
            'consumerA' => [
                'queueManager' => 'testQueueManager',
                'extensions' => [
                    'onEventA' => ['entities' => ['default'], 'priority' => DataCollector::PRIORITY_LOW],
                    'onEventB' => ['entities' => ['default']]
                ]
            ],
        ];
        $configB = $configA;
        $configB['consumerA']['extensions']['onEventA']['priority'] = DataCollector::PRIORITY_URGENT;

        return [
            new DataCollectorState($configA, [], '', DataCollector::PRIORITY_LOW, 'testQueueManager', true),
            new DataCollectorState($configB, [], '', DataCollector::PRIORITY_LOW, 'testQueueManager', true)
        ];
    }
}
