<?php

namespace Phoenix\ReleaseUtil\Tests;

use Phoenix\ReleaseUtil\QueueManager\Command\CommandInterface;
use Phoenix\ReleaseUtil\QueueManager\Command\UndoCommand;

class UndoCommandTest extends \PHPUnit\Framework\TestCase
{
    public function testExecute()
    {
        /** @var CommandInterface&\PHPUnit_Framework_MockObject_MockObject $command */
        $command = $this->createMock(CommandInterface::class);
        $command
            ->expects($this->once())
            ->method('undo');

        $undo = new UndoCommand($command);
        $undo->execute();
    }
}
