<?php

namespace Phoenix\ReleaseUtil\Tests;

use Phoenix\QueueManager\Components\ExchangeData;
use Phoenix\QueueManager\QueueManagerInterface;
use Phoenix\ReleaseUtil\QueueManager\Command\CommandExecutor;
use Phoenix\ReleaseUtil\QueueManager\Command\CommandExecutorLoggerDecorator;
use Phoenix\ReleaseUtil\QueueManager\Command\CreateExchangeCommand;
use Psr\Log\LoggerInterface;

class CommandExecutorLoggerDecoratorTest extends \PHPUnit\Framework\TestCase
{
    public function testExecute()
    {
        $exchange = 'exchange_name';
        $type = 'exchange_type';

        /** @var QueueManagerInterface&\PHPUnit_Framework_MockObject_MockObject $manager */
        $manager = $this->createMock(QueueManagerInterface::class);
        $manager
            ->expects($this->once())
            ->method('createExchange')
            ->with($this->equalTo($exchange), $this->equalTo($type), $this->anything());

        /** @var LoggerInterface&\PHPUnit_Framework_MockObject_MockObject $logger */
        $logger = $this->createMock(LoggerInterface::class);
        $logger
            ->expects($this->atLeastOnce())
            ->method('info')
            ->withAnyParameters();

        $executor = new CommandExecutorLoggerDecorator(new CommandExecutor(), $logger);
        $executor->execute(new CreateExchangeCommand(new ExchangeData($manager, $exchange, $type)));
    }
}
