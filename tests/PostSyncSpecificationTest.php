<?php

namespace Phoenix\ReleaseUtil\Tests;

use Phoenix\ReleaseUtil\QueueManager\Command\BindQueueCommand;
use Phoenix\ReleaseUtil\QueueManager\Command\CreateQueueCommand;
use Phoenix\ReleaseUtil\QueueManager\Command\DeleteExchangeCommand;
use Phoenix\ReleaseUtil\QueueManager\Command\DeleteQueueCommand;
use Phoenix\ReleaseUtil\QueueManager\Specification\PostSyncSpecification;

class PostSyncSpecificationTest extends \PHPUnit\Framework\TestCase
{
    public function testIsSatisfiedBy()
    {
        $commandA = $this->createMock(BindQueueCommand::class);
        $commandB = $this->createMock(CreateQueueCommand::class);
        $commandC = $this->createMock(DeleteExchangeCommand::class);
        $commandD = $this->createMock(DeleteQueueCommand::class);

        $specification = new PostSyncSpecification();
        $result = [];
        foreach ([$commandA, $commandB, $commandC, $commandD] as $command) {
            if ($specification->isSatisfiedBy($command)) {
                $result[] = $command;
            }
        }

        $this->assertEquals([$commandA, $commandC, $commandD], $result);
    }
}
