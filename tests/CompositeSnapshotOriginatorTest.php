<?php

namespace Phoenix\ReleaseUtil\Tests;

use Phoenix\ReleaseUtil\QueueManager\Snapshot\CompositeSnapshotOriginator;
use Phoenix\ReleaseUtil\QueueManager\Snapshot\Snapshot;
use Phoenix\ReleaseUtil\QueueManager\Snapshot\SnapshotOriginatorInterface;

class CompositeSnapshotOriginatorTest extends \PHPUnit\Framework\TestCase
{
    private $originator;
    private $compositeOriginator;

    public function setUp(): void
    {
        $this->originator = $this->createMock(SnapshotOriginatorInterface::class);
        $compositeOriginator = new CompositeSnapshotOriginator();
        $compositeOriginator->addOriginator($this->originator);
        $this->compositeOriginator = $compositeOriginator;
    }

    public function testAddOriginator(): CompositeSnapshotOriginator
    {
        $originator = $this->originator;
        $compositeOriginator = new CompositeSnapshotOriginator();

        $property = new \ReflectionProperty($compositeOriginator, 'originators');
        $property->setAccessible(true);
        $this->assertEmpty($property->getValue($compositeOriginator));

        $compositeOriginator->addOriginator($originator);

        $this->assertContains($originator, $property->getValue($compositeOriginator));

        return $compositeOriginator;
    }

    public function testGetOriginator()
    {
        $restoredOriginator = $this->compositeOriginator->getOriginator(\get_class($this->originator));

        $this->assertEquals($this->originator, $restoredOriginator);
    }

    public function testGetOriginatorEmptyName()
    {
        $this->expectException(\InvalidArgumentException::class);

        $name = '';
        $this->compositeOriginator->getOriginator($name);
    }

    public function testGetOriginatorNotContainsName()
    {
        $this->expectException(\OutOfRangeException::class);

        $name = 'NotContainsName';
        $this->compositeOriginator->getOriginator($name);
    }

    public function testTakeSnapshot()
    {
        $this->originator
            ->expects($this->once())
            ->method('takeSnapshot')
            ->with();

        $snapshot = $this->compositeOriginator->takeSnapshot();

        $this->assertInstanceOf(Snapshot::class, $snapshot);
        $this->assertArrayHasKey(\get_class($this->originator), $snapshot->getState());
    }

    public function testRestore()
    {
        $snapshot = new Snapshot(['text' => 'hello world!']);

        $this->originator
            ->expects($this->once())
            ->method('restore')
            ->with($snapshot);

        $this->compositeOriginator->restore(new Snapshot([\get_class($this->originator) => $snapshot]));
    }
}
