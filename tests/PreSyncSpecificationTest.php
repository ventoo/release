<?php

namespace Phoenix\ReleaseUtil\Tests;

use Phoenix\ReleaseUtil\QueueManager\Command\CreateExchangeCommand;
use Phoenix\ReleaseUtil\QueueManager\Command\CreateQueueCommand;
use Phoenix\ReleaseUtil\QueueManager\Command\DeleteQueueCommand;
use Phoenix\ReleaseUtil\QueueManager\Command\UnbindQueueCommand;
use Phoenix\ReleaseUtil\QueueManager\Specification\PreSyncSpecification;

class PreSyncSpecificationTest extends \PHPUnit\Framework\TestCase
{
    public function testIsSatisfiedBy()
    {
        $commandA = $this->createMock(CreateExchangeCommand::class);
        $commandB = $this->createMock(CreateQueueCommand::class);
        $commandC = $this->createMock(UnbindQueueCommand::class);
        $commandD = $this->createMock(DeleteQueueCommand::class);

        $specification = new PreSyncSpecification();
        $result = [];
        foreach ([$commandA, $commandB, $commandC, $commandD] as $command) {
            if ($specification->isSatisfiedBy($command)) {
                $result[] = $command;
            }
        }

        $this->assertEquals([$commandA, $commandB, $commandC], $result);
    }
}
