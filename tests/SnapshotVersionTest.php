<?php

namespace Phoenix\ReleaseUtil\Tests;

use Phoenix\ReleaseUtil\QueueManager\Snapshot\SnapshotVersion;

class SnapshotVersionTest extends \PHPUnit\Framework\TestCase
{
    public function testGetVersion()
    {
        $valueA = 'test.version';
        $version = new SnapshotVersion($valueA);

        $this->assertEquals($valueA, $version->getVersion());

        $this->expectException(\InvalidArgumentException::class);

        $valueB = '';
        new SnapshotVersion($valueB);
    }
}
