<?php

namespace Phoenix\ReleaseUtil\Tests;

use Phoenix\ReleaseUtil\QueueManager\Command\CommandCollection;
use Phoenix\ReleaseUtil\QueueManager\Command\CommandExecutor;
use Phoenix\ReleaseUtil\QueueManager\Command\CreateExchangeCommand;
use Phoenix\ReleaseUtil\QueueManager\ReleaseCommandExecutor;
use Phoenix\ReleaseUtil\QueueManager\Snapshot\Snapshot;

class ReleaseCommandExecutorTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @dataProvider dataProvider
     * @throws \ReflectionException
     */
    public function testExecute(ReleaseCommandExecutor $executor, CommandCollection $commands)
    {
        /** @var CreateExchangeCommand&\PHPUnit_Framework_MockObject_MockObject $command */
        foreach ($commands as $command) {
            $command
                ->expects($this->once())
                ->method('execute')
                ->with();
            $executor->execute($command);
        }
        /** @var CommandCollection $executorCommands */
        $executorCommands = $this->getPrivateProperty($executor, 'commands')->getValue($executor);

        $this->assertTrue($commands->equals($executorCommands));

        $count = $executorCommands->count();
        for ($i = 0; $i < $count; $i++) {
            $executorCommands->getAt($i)->equals($commands->getAt($i));
        }
    }

    /**
     * @dataProvider dataProvider
     * @throws \ReflectionException
     */
    public function testRevert(ReleaseCommandExecutor $executor, CommandCollection $commands)
    {
        /** @var CreateExchangeCommand&\PHPUnit_Framework_MockObject_MockObject $command */
        foreach ($commands as $command) {
            $command
                ->expects($this->once())
                ->method('undo')
                ->with();
        }
        $this->getPrivateProperty($executor, 'commands')->setValue($executor, $commands);
        $executor->revert();
        $executorCommands = $this->getPrivateProperty($executor, 'commands')->getValue($executor);

        $this->assertTrue($commands->equals($executorCommands));
    }

    /**
     * @dataProvider dataProvider
     * @throws \ReflectionException
     */
    public function testReplay(ReleaseCommandExecutor $executor, CommandCollection $commands)
    {
        /** @var CreateExchangeCommand&\PHPUnit_Framework_MockObject_MockObject $command */
        foreach ($commands as $command) {
            $command
                ->expects($this->once())
                ->method('execute')
                ->with();
        }
        $this->getPrivateProperty($executor, 'commands')->setValue($executor, $commands);
        $executor->replay();
        $executorCommands = $this->getPrivateProperty($executor, 'commands')->getValue($executor);

        $this->assertTrue($commands->equals($executorCommands));
    }

    /**
     * @dataProvider dataProvider
     * @throws \ReflectionException
     */
    public function testTakeSnapshot(ReleaseCommandExecutor $executor, CommandCollection $commands)
    {
        $snapshot = $executor->takeSnapshot();

        $this->assertInstanceOf(Snapshot::class, $snapshot);

        /** @var CommandCollection $state */
        $state = $snapshot->getState();

        $this->assertInstanceOf(CommandCollection::class, $state);
        $this->assertTrue($state->isEmpty());

        $this->getPrivateProperty($executor, 'commands')->setValue($executor, $commands);

        $this->assertTrue($commands->equals($executor->takeSnapshot()->getState()));
    }

    /**
     * @dataProvider dataProvider
     * @throws \ReflectionException
     */
    public function testRestore(ReleaseCommandExecutor $executor, CommandCollection $commands)
    {
        $stateA = new Snapshot($commands);
        $executor->restore($stateA);
        $executorCommands = $this->getPrivateProperty($executor, 'commands')->getValue($executor);

        $this->assertTrue($commands->equals($executorCommands));

        $this->expectException(\DomainException::class);

        $stateB = ['text' => 'hello world!'];
        $executor->restore(new Snapshot($stateB));
    }

    public function dataProvider()
    {
        $executor = new ReleaseCommandExecutor(new CommandExecutor());
        $commands = new CommandCollection();
        for ($i = 0; $i < 3; $i++) {
            /** @var CreateExchangeCommand&\PHPUnit_Framework_MockObject_MockObject $command */
            $command = $this->getMockBuilder(CreateExchangeCommand::class)
                ->disableOriginalConstructor()
                ->setMethods(['execute', 'undo'])
                ->getMock();
            $commands->add($command);
        }

        return [[$executor, $commands]];
    }

    /**
     * @throws \ReflectionException
     */
    private function getPrivateProperty($class, $name): \ReflectionProperty
    {
        $property = new \ReflectionProperty($class, $name);
        $property->setAccessible(true);

        return $property;
    }
}
