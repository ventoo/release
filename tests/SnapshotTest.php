<?php

namespace Phoenix\ReleaseUtil\Tests;

use Phoenix\ReleaseUtil\QueueManager\Snapshot\Snapshot;

class SnapshotTest extends \PHPUnit\Framework\TestCase
{
    public function testGetState()
    {
        $state = ['text' => 'hello world!'];
        $snapshot = new Snapshot($state);

        $this->assertEquals($state, $snapshot->getState());
    }
}
