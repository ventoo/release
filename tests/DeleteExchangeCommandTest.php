<?php

namespace Phoenix\ReleaseUtil\Tests;

use Phoenix\QueueManager\Components\ExchangeData;
use Phoenix\QueueManager\QueueManagerInterface;
use Phoenix\ReleaseUtil\QueueManager\Command\DeleteExchangeCommand;

class DeleteExchangeCommandTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var QueueManagerInterface&\PHPUnit_Framework_MockObject_MockObject
     */
    protected $manager;

    protected function setUp(): void
    {
        $this->manager = $this->createMock(QueueManagerInterface::class);
        $this->manager->exchange_default = '';
    }

    /**
     * @dataProvider dataProvider
     */
    public function testExecute(string $exchange, string $type, array $params)
    {
        $this->manager
            ->expects($this->once())
            ->method('deleteExchange')
            ->with($this->equalTo($exchange), $this->equalTo(DeleteExchangeCommand::DELETE_IF_UNUSED));

        $command = new DeleteExchangeCommand(new ExchangeData($this->manager, $exchange, $type, $params));
        $command->execute();
    }

    /**
     * @dataProvider dataProvider
     */
    public function testUndo(string $exchange, string $type, array $params)
    {
        $this->manager
            ->expects($this->once())
            ->method('createExchange')
            ->with($this->equalTo($exchange), $this->equalTo($type), $this->equalTo($params));

        $command = new DeleteExchangeCommand(new ExchangeData($this->manager, $exchange, $type, $params));
        $command->undo();
    }

    public function dataProvider()
    {
        return [['exchange_name', 'exchange_type', ['passive' => false, 'durable' => true, 'autoDelete' => false]]];
    }
}
