<?php

namespace Phoenix\ReleaseUtil\Tests;

use Phoenix\ReleaseUtil\QueueManager\Repository\SnapshotRepositoryInterface;
use Phoenix\ReleaseUtil\QueueManager\Snapshot\SnapshotVersion;
use Phoenix\ReleaseUtil\QueueManager\Snapshot\SnapshotVersionCollection;
use Phoenix\ReleaseUtil\QueueManager\SnapshotRotator;

class SnapshotRotatorTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var SnapshotRepositoryInterface&\PHPUnit_Framework_MockObject_MockObject
     */
    protected $repository;

    protected function setUp(): void
    {
        $this->repository = $this->createMock(SnapshotRepositoryInterface::class);
    }

    /**
     * @dataProvider dataProvider
     */
    public function testRotate(SnapshotVersionCollection $versions, int $count, int $headCount, array $headVersions)
    {
        $this->repository
            ->method('getVersions')
            ->willReturn($versions);
        $this->repository
            ->expects($this->exactly($headCount))
            ->method('delete')
            ->withConsecutive(...$headVersions);

        $rotator = new SnapshotRotator($this->repository);
        $rotator->rotate($count);
    }

    public function testRotateInvalidCount()
    {
        $this->expectException(\InvalidArgumentException::class);

        $rotator = new SnapshotRotator($this->repository);
        $rotator->rotate(-2);
    }

    public function dataProvider()
    {
        $versions = new SnapshotVersionCollection();
        $versions->add(new SnapshotVersion('test_version_0'));
        $versions->add(new SnapshotVersion('test_version_1'));
        $versions->add(new SnapshotVersion('test_version_2'));
        $versions->add(new SnapshotVersion('test_version_3'));
        $versions->add(new SnapshotVersion('test_version_4'));

        return [
            [
                $versions,
                0,
                5,
                [
                    [$versions->getAt(0)],
                    [$versions->getAt(1)],
                    [$versions->getAt(2)],
                    [$versions->getAt(3)],
                    [$versions->getAt(4)]
                ]
            ],
            [
                $versions,
                2,
                3,
                [
                    [$versions->getAt(0)],
                    [$versions->getAt(1)],
                    [$versions->getAt(2)]
                ]
            ],
            [
            $versions,
                7,
                0,
                [
                ]
            ]
        ];
    }
}
