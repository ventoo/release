<?php

namespace Phoenix\ReleaseUtil\Tests;

use Phoenix\QueueManager\Components\QueueData;
use Phoenix\QueueManager\QueueManagerInterface;
use Phoenix\ReleaseUtil\QueueManager\Command\BindQueueCommand;

class BindQueueCommandTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var QueueManagerInterface&\PHPUnit_Framework_MockObject_MockObject
     */
    protected $manager;

    protected function setUp(): void
    {
        $this->manager = $this->createMock(QueueManagerInterface::class);
        $this->manager
            ->method('getQueueName')
            ->will($this->returnArgument(0));
        $this->manager->exchange_default = '';
    }

    /**
     * @dataProvider dataProvider
     */
    public function testExecute(string $queue, string $exchange, string $routingKey)
    {
        $this->manager
            ->expects($this->once())
            ->method('bindQueue')
            ->with($this->equalTo($queue), $this->equalTo($routingKey), $this->equalTo($exchange));

        $command = new BindQueueCommand(new QueueData($this->manager, $queue), [compact('routingKey', 'exchange')]);
        $command->execute();
    }

    /**
     * @dataProvider dataProvider
     */
    public function testUndo(string $queue, string $exchange, string $routingKey)
    {
        $this->manager
            ->expects($this->once())
            ->method('unbindQueue')
            ->with($this->equalTo($queue), $this->equalTo($routingKey), $this->equalTo($exchange));

        $command = new BindQueueCommand(new QueueData($this->manager, $queue), [compact('routingKey', 'exchange')]);
        $command->undo();
    }

    public function dataProvider()
    {
        return [['queue_name', 'exchange_name', 'routing_key']];
    }
}
