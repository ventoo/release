<?php

namespace Phoenix\ReleaseUtil\Tests;

use Phoenix\QueueManager\Components\QueueData;
use Phoenix\QueueManager\QueueManagerInterface;
use Phoenix\ReleaseUtil\QueueManager\Command\DeleteQueueCommand;

class DeleteQueueCommandTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var QueueManagerInterface&\PHPUnit_Framework_MockObject_MockObject
     */
    protected $manager;

    protected function setUp(): void
    {
        $this->manager = $this->createMock(QueueManagerInterface::class);
        $this->manager
            ->method('getQueueName')
            ->will($this->returnArgument(0));
        $this->manager->exchange_default = '';
    }

    /**
     * @dataProvider dataProvider
     */
    public function testExecute(string $queue)
    {
        $this->manager
            ->expects($this->once())
            ->method('deleteQueue')
            ->with($this->equalTo($queue));

        $command = new DeleteQueueCommand(new QueueData($this->manager, $queue));
        $command->execute();
    }

    /**
     * @dataProvider dataProvider
     */
    public function testUndo(string $queue, $params, $bindings)
    {
        $data = new QueueData($this->manager, $queue, $params, $bindings);

        $this->manager
            ->expects($this->once())
            ->method('createQueue')
            ->with($this->equalTo($queue), $this->equalTo($params), $this->equalTo($data->getBindings()));

        $command = new DeleteQueueCommand($data);
        $command->undo();
    }

    public function dataProvider()
    {
        return [
            [
                'queue_name',
                ['passive' => false, 'durable' => true, 'exclusive' => false, 'autoDelete' => false, 'arguments' => []],
                [['routingKey' => 'routing_key', 'exchange' => 'exchange_name']]
            ]
        ];
    }
}
