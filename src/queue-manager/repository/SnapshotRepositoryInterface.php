<?php

namespace Phoenix\ReleaseUtil\QueueManager\Repository;

use Phoenix\ReleaseUtil\QueueManager\Snapshot\Snapshot;
use Phoenix\ReleaseUtil\QueueManager\Snapshot\SnapshotVersion;
use Phoenix\ReleaseUtil\QueueManager\Snapshot\SnapshotVersionCollection;

interface SnapshotRepositoryInterface
{
    public function persist(SnapshotVersion $version, Snapshot $snapshot): void;

    public function retrieve(SnapshotVersion $version): Snapshot;

    public function delete(SnapshotVersion $version): void;

    public function getVersions(): SnapshotVersionCollection;
}
