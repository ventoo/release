<?php

namespace Phoenix\ReleaseUtil\QueueManager\Repository;

use Phoenix\ReleaseUtil\QueueManager\Snapshot\Snapshot;
use Phoenix\ReleaseUtil\QueueManager\Snapshot\SnapshotVersion;
use Phoenix\ReleaseUtil\QueueManager\Snapshot\SnapshotVersionCollection;

class StreamSnapshotRepository implements SnapshotRepositoryInterface
{
    private $path;

    public function __construct(string $path)
    {
        $this->ensureIsValidPath($path);
        $this->path = $path;
    }

    public function persist(SnapshotVersion $version, Snapshot $snapshot): void
    {
        $url = \sprintf('%s/%s.log', $this->path, $version->getVersion());
        try {
            \file_put_contents($url, \serialize($snapshot));
        } catch (\Throwable $e) {
            throw new \RuntimeException(\sprintf('Error while writing to resource "%s": %s', $url, $e->getMessage()));
        }
    }

    public function retrieve(SnapshotVersion $version): Snapshot
    {
        $url = \sprintf('%s/%s.log', $this->path, $version->getVersion());
        try {
            $contents = \file_get_contents($url);
        } catch (\Throwable $e) {
            throw new \RuntimeException(\sprintf('Error while reading resource "%s": %s', $url, $e->getMessage()));
        }

        return \unserialize($contents);
    }

    public function delete(SnapshotVersion $version): void
    {
        $url = \sprintf('%s/%s.log', $this->path, $version->getVersion());
        try {
            \unlink($url);
        } catch (\Throwable $e) {
            throw new \RuntimeException(\sprintf('Error deleting resource "%s": %s', $url, $e->getMessage()));
        }
    }

    public function getVersions(): SnapshotVersionCollection
    {
        $pattern = \sprintf('%s/*.log', $this->path);
        try {
            $pathnames = \glob($pattern, GLOB_ERR);
        } catch (\Throwable $e) {
            throw new \RuntimeException(
                \sprintf('Error during search for the pathnames "%s": %s', $pattern, $e->getMessage())
            );
        }
        $collection = new SnapshotVersionCollection();
        foreach ($pathnames as $pathname) {
            $collection->add(new SnapshotVersion(\basename($pathname, '.log')));
        }

        return $collection;
    }

    private function ensureIsValidPath(string $path): void
    {
        if ('' === $path) {
            throw new \InvalidArgumentException(\sprintf('Invalid path given "%s"', $path));
        }
    }
}
