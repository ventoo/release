<?php

namespace Phoenix\ReleaseUtil\QueueManager\Specification;

use Phoenix\ReleaseUtil\QueueManager\Command\BindQueueCommand;
use Phoenix\ReleaseUtil\QueueManager\Command\CommandInterface;
use Phoenix\ReleaseUtil\QueueManager\Command\DeleteExchangeCommand;
use Phoenix\ReleaseUtil\QueueManager\Command\DeleteQueueCommand;
use Phoenix\Utils\CompositeSpecification;

class PostSyncSpecification extends CompositeSpecification
{
    const AVAILABLE_COMMAND_LIST = [
        BindQueueCommand::class,
        DeleteQueueCommand::class,
        DeleteExchangeCommand::class
    ];

    /**
     * @param CommandInterface $candidate
     * @return bool
     */
    public function isSatisfiedBy($candidate): bool
    {
        foreach (self::AVAILABLE_COMMAND_LIST as $command) {
            if (\is_a($candidate, $command)) {
                return true;
            }
        }

        return false;
    }
}
