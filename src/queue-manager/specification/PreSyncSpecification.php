<?php

namespace Phoenix\ReleaseUtil\QueueManager\Specification;

use Phoenix\ReleaseUtil\QueueManager\Command\CommandInterface;
use Phoenix\ReleaseUtil\QueueManager\Command\CreateExchangeCommand;
use Phoenix\ReleaseUtil\QueueManager\Command\CreateQueueCommand;
use Phoenix\ReleaseUtil\QueueManager\Command\UnbindQueueCommand;
use Phoenix\Utils\CompositeSpecification;

class PreSyncSpecification extends CompositeSpecification
{
    const AVAILABLE_COMMAND_LIST = [
        CreateExchangeCommand::class,
        CreateQueueCommand::class,
        UnbindQueueCommand::class
    ];

    /**
     * @param CommandInterface $candidate
     * @return bool
     */
    public function isSatisfiedBy($candidate): bool
    {
        foreach (self::AVAILABLE_COMMAND_LIST as $command) {
            if (\is_a($candidate, $command)) {
                return true;
            }
        }

        return false;
    }
}
