<?php

namespace Phoenix\ReleaseUtil\QueueManager;

use Phoenix\ReleaseUtil\QueueManager\Snapshot\SnapshotManagerInterface;
use Phoenix\ReleaseUtil\QueueManager\Snapshot\SnapshotOriginatorInterface;
use Phoenix\ReleaseUtil\QueueManager\Snapshot\SnapshotVersion;
use Phoenix\ReleaseUtil\QueueManager\Snapshot\SnapshotVersionCollection;
use Psr\Log\LoggerInterface;

class ReleaseSnapshotManagerLoggerDecorator implements SnapshotManagerInterface
{
    private $manager;

    private $logger;

    public function __construct(SnapshotManagerInterface $manager, LoggerInterface $logger)
    {
        $this->manager = $manager;
        $this->logger = $logger;
    }

    public function takeSnapshot(SnapshotVersion $version): void
    {
        $this->logger->info(\sprintf('Taking release snapshot version "%s"', $version->getVersion()));
        $this->manager->takeSnapshot($version);
        $this->logger->info(\sprintf('Snapshot version "%s" successfully stored', $version->getVersion()));
    }

    public function loadSnapshot(SnapshotVersion $version): void
    {
        $this->logger->info(\sprintf('Loading release snapshot version "%s"', $version->getVersion()));
        $this->manager->loadSnapshot($version);
        $this->logger->info(\sprintf('Snapshot version "%s" successfully loaded', $version->getVersion()));
    }

    public function deleteSnapshot(SnapshotVersion $version): void
    {
        $this->logger->info(\sprintf('Deleting release snapshot version "%s"', $version->getVersion()));
        $this->manager->takeSnapshot($version);
        $this->logger->info(\sprintf('Snapshot version "%s" successfully deleted', $version->getVersion()));
    }

    public function getOriginator(string $name): SnapshotOriginatorInterface
    {
        return $this->manager->getOriginator($name);
    }

    public function getVersions(): SnapshotVersionCollection
    {
        return $this->manager->getVersions();
    }
}
