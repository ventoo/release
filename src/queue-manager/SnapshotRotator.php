<?php

namespace Phoenix\ReleaseUtil\QueueManager;

use Phoenix\ReleaseUtil\QueueManager\Repository\SnapshotRepositoryInterface;

class SnapshotRotator
{
    private $repository;

    public function __construct(SnapshotRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function rotate(int $count): void
    {
        $this->ensureIsValidCount($count);
        $versions = $this->repository->getVersions();
        $headCount = $versions->count() - $count;
        if ($headCount > 0) {
            $headVersions = $versions->stream()->limit($headCount);
            foreach ($headVersions as $version) {
                $this->repository->delete($version);
            }
        }
    }

    private function ensureIsValidCount(int $count): void
    {
        if ($count < 0) {
            throw new \InvalidArgumentException('Count can`t be less than 0');
        }
    }
}
