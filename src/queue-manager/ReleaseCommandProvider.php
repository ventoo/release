<?php

namespace Phoenix\ReleaseUtil\QueueManager;

use Phoenix\ReleaseUtil\QueueManager\Command\CommandCollection;
use Phoenix\ReleaseUtil\QueueManager\Command\CommandProviderInterface;
use Phoenix\Utils\SpecificationInterface;

class ReleaseCommandProvider implements CommandProviderInterface
{
    /**
     * @var CommandProviderInterface[]
     */
    private $providers = [];

    public function addProvider(CommandProviderInterface $provider): ReleaseCommandProvider
    {
        $this->providers[] = $provider;

        return $this;
    }

    public function getAll(): CommandCollection
    {
        $commands = new CommandCollection();
        foreach ($this->providers as $provider) {
            $commands->addAll($provider->getAll());
        }

        return $commands;
    }

    public function getBySpecification(SpecificationInterface $specification): CommandCollection
    {
        $specified = $this->getAll()->copy();
        $specified->retainIf($specification);

        return $specified;
    }
}
