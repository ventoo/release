<?php

namespace Phoenix\ReleaseUtil\QueueManager;

use Phoenix\ReleaseUtil\QueueManager\Command\CommandCollection;
use Phoenix\ReleaseUtil\QueueManager\Command\CommandExecutorInterface;
use Phoenix\ReleaseUtil\QueueManager\Command\CommandInterface;
use Phoenix\ReleaseUtil\QueueManager\Command\UndoCommand;
use Phoenix\ReleaseUtil\QueueManager\Snapshot\Snapshot;
use Phoenix\ReleaseUtil\QueueManager\Snapshot\SnapshotOriginatorInterface;

class ReleaseCommandExecutor implements CommandExecutorInterface, SnapshotOriginatorInterface
{
    private $executor;

    private $commands;

    public function __construct(CommandExecutorInterface $executor)
    {
        $this->executor = $executor;
        $this->commands = new CommandCollection();
    }

    public function replay(): void
    {
        foreach ($this->commands as $command) {
            $this->executeCommand($command);
        }
    }

    public function revert(): void
    {
        if ($this->commands->isEmpty()) {
            return;
        }
        $count = $this->commands->count();
        for ($i = --$count; $i >= 0; $i--) {
            $this->executeCommand(new UndoCommand($this->commands->getAt($i)));
        }
    }

    public function takeSnapshot(): Snapshot
    {
        return new Snapshot($this->commands);
    }

    public function restore(Snapshot $snapshot): SnapshotOriginatorInterface
    {
        $state = $snapshot->getState();
        if (!$state instanceof CommandCollection) {
            throw new \DomainException(\sprintf('State must be instance of %s', CommandCollection::class));
        }
        $this->commands = $state;

        return $this;
    }

    public function execute(CommandInterface $command): void
    {
        $this->executeAndStoreCommand($command);
    }

    private function executeCommand(CommandInterface $command): void
    {
        $this->executor->execute($command);
    }

    private function executeAndStoreCommand(CommandInterface $command): void
    {
        $this->executeCommand($command);
        $this->commands->add($command);
    }
}
