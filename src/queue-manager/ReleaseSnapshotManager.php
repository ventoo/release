<?php

namespace Phoenix\ReleaseUtil\QueueManager;

use Phoenix\ReleaseUtil\QueueManager\Repository\SnapshotRepositoryInterface;
use Phoenix\ReleaseUtil\QueueManager\Snapshot\CompositeSnapshotOriginator;
use Phoenix\ReleaseUtil\QueueManager\Snapshot\SnapshotManagerInterface;
use Phoenix\ReleaseUtil\QueueManager\Snapshot\SnapshotOriginatorInterface;
use Phoenix\ReleaseUtil\QueueManager\Snapshot\SnapshotVersion;
use Phoenix\ReleaseUtil\QueueManager\Snapshot\SnapshotVersionCollection;

class ReleaseSnapshotManager implements SnapshotManagerInterface
{
    private $originators;

    private $repository;

    public function __construct(CompositeSnapshotOriginator $originators, SnapshotRepositoryInterface $repository)
    {
        $this->originators = $originators;
        $this->repository = $repository;
    }

    public function takeSnapshot(SnapshotVersion $version): void
    {
        $this->repository->persist($version, $this->originators->takeSnapshot());
    }

    public function loadSnapshot(SnapshotVersion $version): void
    {
        $this->originators = $this->originators->restore($this->repository->retrieve($version));
    }

    public function deleteSnapshot(SnapshotVersion $version): void
    {
        $this->repository->delete($version);
    }

    public function getOriginator(string $name): SnapshotOriginatorInterface
    {
        return $this->originators->getOriginator($name);
    }

    public function getVersions(): SnapshotVersionCollection
    {
        return $this->repository->getVersions();
    }
}
