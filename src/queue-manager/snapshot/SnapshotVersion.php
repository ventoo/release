<?php

namespace Phoenix\ReleaseUtil\QueueManager\Snapshot;

use Phoenix\Utils\IdenticalInterface;
use Phoenix\Utils\Values;

class SnapshotVersion implements IdenticalInterface
{
    private $version;

    public function __construct(string $version)
    {
        $this->ensureIsValidVersion($version);
        $this->version = $version;
    }

    public function getVersion(): string
    {
        return $this->version;
    }

    public function equals(IdenticalInterface $version): bool
    {
        return $version instanceof self && $this->version === $version->version;
    }

    public function hashCode()
    {
        return Values::hashCode($this->version);
    }

    private function ensureIsValidVersion(string $version): void
    {
        if ('' === $version) {
            throw new \InvalidArgumentException(\sprintf('Invalid version given "%s"', $version));
        }
    }
}
