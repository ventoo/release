<?php

namespace Phoenix\ReleaseUtil\QueueManager\Snapshot;

class Snapshot
{
    private $state;

    public function __construct($state)
    {
        $this->state = $state;
    }

    public function getState()
    {
        return $this->state;
    }
}
