<?php

namespace Phoenix\ReleaseUtil\QueueManager\Snapshot;

class CompositeSnapshotOriginator implements SnapshotOriginatorInterface
{
    /**
     * @var SnapshotOriginatorInterface[]
     */
    private $originators = [];

    public function addOriginator(SnapshotOriginatorInterface $originator): CompositeSnapshotOriginator
    {
        $name = \get_class($originator);
        $this->originators[$name] = $originator;

        return $this;
    }

    public function getOriginator(string $name): SnapshotOriginatorInterface
    {
        $this->ensureIsValidName($name);
        if (!isset($this->originators[$name])) {
            throw new \OutOfRangeException(\sprintf('Originator "%s" not found', $name));
        }

        return $this->originators[$name];
    }

    public function takeSnapshot(): Snapshot
    {
        $state = [];
        foreach ($this->originators as $name => $originator) {
            $state[$name] = $originator->takeSnapshot();
        }

        return new Snapshot($state);
    }

    public function restore(Snapshot $snapshot): SnapshotOriginatorInterface
    {
        $state = $snapshot->getState();
        $this->ensureIsValidState($state);
        foreach ($state as $name => $snapshot) {
            $this->originators[$name] = $this->getOriginator($name)->restore($snapshot);
        }

        return $this;
    }

    private function ensureIsValidName(string $name): void
    {
        if ('' === $name) {
            throw new \InvalidArgumentException('Name can\'t be empty');
        }
    }

    private function ensureIsValidState($state): void
    {
        if (!\is_array($state)) {
            throw new \DomainException('Invalid snapshot state, array expected');
        }
    }
}
