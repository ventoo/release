<?php

namespace Phoenix\ReleaseUtil\QueueManager\Snapshot;

use Phoenix\Utils\AbstractList;

/**
 * @method bool add(SnapshotVersion $element)
 * @method bool contains(SnapshotVersion $element)
 * @method bool remove(SnapshotVersion $element)
 * @method SnapshotVersion getAt(int $index) throws exception when $index out of range
 * @method void addAt(int $index, SnapshotVersion $element) throws exception when $index out of range
 * @method SnapshotVersion setAt(int $index, SnapshotVersion $element) throws exception when $index out of range
 * @method SnapshotVersion removeAt(int $index) throws exception when when $index out of range
 * @method int indexOf(SnapshotVersion $element)
 * @method int lastIndexOf(SnapshotVersion $element)
 * @method SnapshotVersion[]|\Iterator getIterator()
 * @method SnapshotVersion first() throws exception when empty
 * @method SnapshotVersionCollection copy()
 */
class SnapshotVersionCollection extends AbstractList
{
    public function isValidElement($element): bool
    {
        return $element instanceof SnapshotVersion;
    }
}
