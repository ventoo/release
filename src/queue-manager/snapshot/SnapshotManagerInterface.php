<?php

namespace Phoenix\ReleaseUtil\QueueManager\Snapshot;

interface SnapshotManagerInterface
{
    public function takeSnapshot(SnapshotVersion $version): void;

    public function loadSnapshot(SnapshotVersion $version): void;

    public function deleteSnapshot(SnapshotVersion $version): void;

    public function getOriginator(string $name): SnapshotOriginatorInterface;

    public function getVersions(): SnapshotVersionCollection;
}
