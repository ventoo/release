<?php

namespace Phoenix\ReleaseUtil\QueueManager\Snapshot;

interface SnapshotOriginatorInterface
{
    public function takeSnapshot(): Snapshot;

    public function restore(Snapshot $snapshot): SnapshotOriginatorInterface;
}
