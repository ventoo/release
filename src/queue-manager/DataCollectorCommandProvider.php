<?php

namespace Phoenix\ReleaseUtil\QueueManager;

use Phoenix\DataCollector;
use Phoenix\QueueManager\Components\EQueueManager;
use Phoenix\QueueManager\Components\ExchangeData;
use Phoenix\QueueManager\Components\QueueData;
use Phoenix\ReleaseUtil\QueueManager\Command\BindQueueCommand;
use Phoenix\ReleaseUtil\QueueManager\Command\CommandCollection;
use Phoenix\ReleaseUtil\QueueManager\Command\CommandProviderInterface;
use Phoenix\ReleaseUtil\QueueManager\Command\CreateExchangeCommand;
use Phoenix\ReleaseUtil\QueueManager\Command\CreateQueueCommand;
use Phoenix\ReleaseUtil\QueueManager\Command\DeleteExchangeCommand;
use Phoenix\ReleaseUtil\QueueManager\Command\DeleteQueueCommand;
use Phoenix\ReleaseUtil\QueueManager\Command\UnbindQueueCommand;
use Phoenix\Utils\Values;

class DataCollectorCommandProvider implements CommandProviderInterface
{
    private $current;

    private $manager;

    /**
     * @var DataCollector
     */
    private $previous;

    public function __construct(DataCollector $dataCollector, ReleaseSnapshotManager $manager)
    {
        $this->current = $dataCollector;
        $this->manager = $manager;
    }

    public function getAll(): CommandCollection
    {
        $this->previous = $this->manager->getOriginator(DataCollector::class);

        return $this->getCommands();
    }

    private function getCommands(): CommandCollection
    {
        $commands = new CommandCollection();
        $currExchangeData = $this->getCurrentExchangeData();
        $prevExchangeData = $this->getPreviousExchangeData();
        $newExchanges = \array_diff_key($currExchangeData, $prevExchangeData);
        foreach ($newExchanges as $exchangeData) {
            $commands->add(new CreateExchangeCommand($exchangeData));
        }
        $missingExchanges = \array_diff_key($prevExchangeData, $currExchangeData);
        foreach ($missingExchanges as $exchangeData) {
            $commands->add(new DeleteExchangeCommand($exchangeData));
        }
        $currQueueData = $this->getCurrentQueueData();
        $prevQueueData = $this->getPreviousQueueData();
        foreach ($currQueueData as $hashCode => $queueData) {
            $isNewConsumer = !isset($prevQueueData[$hashCode]);
            if ($isNewConsumer) {
                $commands->add(new CreateQueueCommand($queueData));
            } else {
                $currBindings = $queueData->getBindings();
                $prevBindings = $prevQueueData[$hashCode]->getBindings();
                $newBindings = [];
                foreach ($currBindings as $binding) {
                    if (!\in_array($binding, $prevBindings)) {
                        $newBindings[] = $binding;
                    }
                }
                if (\count($newBindings)) {
                    $commands->add(new BindQueueCommand($queueData, $newBindings));
                }
                $missingBindings = [];
                foreach ($prevBindings as $binding) {
                    if (!\in_array($binding, $currBindings)) {
                        $missingBindings[] = $binding;
                    }
                }
                if (\count($missingBindings)) {
                    $commands->add(new UnbindQueueCommand($queueData, $missingBindings));
                }
            }
        }
        $missingConsumers = \array_diff_key($prevQueueData, $currQueueData);
        foreach ($missingConsumers as $queueData) {
            $commands->add(new DeleteQueueCommand($queueData));
        }

        return $commands;
    }

    /**
     * @return ExchangeData[]
     */
    private function getCurrentExchangeData(): array
    {
        return $this->getExchangeData($this->current);
    }

    /**
     * @return ExchangeData[]
     */
    private function getPreviousExchangeData(): array
    {
        return $this->getExchangeData($this->previous);
    }

    /**
     * @param DataCollector $dataCollector
     * @return ExchangeData[]
     */
    private function getExchangeData(DataCollector $dataCollector): array
    {
        $result = [];
        /** @var ExchangeData $exchangeData */
        foreach ($dataCollector->getExchangeData() as $exchangeData) {
            /** @var EQueueManager $manager */
            $manager = $exchangeData->getManager();
            $hashCode = Values::hash($manager->client_key, $exchangeData->getName(), $exchangeData->getType());
            $result[$hashCode] = $exchangeData;
        }

        return $result;
    }

    /**
     * @return QueueData[]
     */
    private function getCurrentQueueData(): array
    {
        return $this->getQueueData($this->current);
    }

    /**
     * @return QueueData[]
     */
    private function getPreviousQueueData(): array
    {
        return $this->getQueueData($this->previous);
    }

    /**
     * @param DataCollector $dataCollector
     * @return QueueData[]
     */
    private function getQueueData(DataCollector $dataCollector): array
    {
        $result = [];
        /** @var QueueData $queueData */
        foreach ($dataCollector->getQueueData() as $queueData) {
            /** @var EQueueManager $manager */
            $manager = $queueData->getManager();
            $parts = \explode('-', $queueData->getName());
            $consumer = \end($parts);
            if (\property_exists($manager, 'client_key')) {
                $hashCode = Values::hash($manager->client_key, $consumer);
            } else {
                $hashCode = Values::hash($dataCollector->config[$consumer]['queueManager'], $consumer);
            }
            $result[$hashCode] = $queueData;
        }

        return $result;
    }
}
