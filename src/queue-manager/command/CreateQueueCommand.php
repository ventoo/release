<?php

namespace Phoenix\ReleaseUtil\QueueManager\Command;

use Phoenix\QueueManager\Components\QueueData;
use Phoenix\Utils\IdenticalTrait;

class CreateQueueCommand implements CommandInterface
{
    use IdenticalTrait;

    private $data;

    public function __construct(QueueData $data)
    {
        $this->data = $data;
    }

    public function execute(): void
    {
        $this->data->createQueue();
    }

    public function undo(): void
    {
        $this->data->deleteQueue();
    }

    public function __toString()
    {
        return \sprintf('queue "%s"', $this->data->getName());
    }
}
