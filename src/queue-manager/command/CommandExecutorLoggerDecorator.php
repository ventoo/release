<?php

namespace Phoenix\ReleaseUtil\QueueManager\Command;

use Psr\Log\LoggerInterface;

class CommandExecutorLoggerDecorator implements CommandExecutorInterface
{
    private $executor;

    private $logger;

    public function __construct(CommandExecutorInterface $executor, LoggerInterface $logger)
    {
        $this->executor = $executor;
        $this->logger = $logger;
    }

    public function execute(CommandInterface $command): void
    {
        $this->log($command);
        $this->executor->execute($command);
    }

    private function log(CommandInterface $command): void
    {
        $name = $this->getCommandName($command);
        $this->logger->info(\sprintf('Executing command "%s": %s', $name, $command));
    }

    private function getCommandName(CommandInterface $command): string
    {
        try {
            $reflection = new \ReflectionClass($command);
            $name = $reflection->getShortName();
        } catch (\ReflectionException $e) {
            $name = 'UndefinedCommand';
        }

        return $name;
    }
}
