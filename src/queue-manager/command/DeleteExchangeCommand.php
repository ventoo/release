<?php

namespace Phoenix\ReleaseUtil\QueueManager\Command;

use Phoenix\QueueManager\Components\ExchangeData;
use Phoenix\Utils\IdenticalTrait;

class DeleteExchangeCommand implements CommandInterface
{
    use IdenticalTrait;

    const DELETE_IF_UNUSED = true;

    private $data;

    public function __construct(ExchangeData $data)
    {
        $this->data = $data;
    }

    public function execute(): void
    {
        $this->data->deleteExchange(self::DELETE_IF_UNUSED);
    }

    public function undo(): void
    {
        $this->data->createExchange();
    }

    public function __toString()
    {
        return \sprintf('exchange "%s" type "%s"', $this->data->getName(), $this->data->getType());
    }
}
