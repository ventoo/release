<?php

namespace Phoenix\ReleaseUtil\QueueManager\Command;

class CommandExecutor implements CommandExecutorInterface
{
    public function execute(CommandInterface $command): void
    {
        $command->execute();
    }
}
