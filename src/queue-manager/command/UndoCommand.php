<?php

namespace Phoenix\ReleaseUtil\QueueManager\Command;

use Phoenix\Utils\IdenticalTrait;

class UndoCommand implements CommandInterface
{
    use IdenticalTrait;

    private $command;

    public function __construct(CommandInterface $command)
    {
        $this->command = $command;
    }

    public function execute(): void
    {
        $this->command->undo();
    }

    public function undo(): void
    {
    }

    private function getCommandName(CommandInterface $command): string
    {
        try {
            $reflection = new \ReflectionClass($command);
            $name = $reflection->getShortName();
        } catch (\ReflectionException $e) {
            $name = 'UndefinedCommand';
        }

        return $name;
    }

    public function __toString()
    {
        $commandName = $this->getCommandName($this->command);

        return \sprintf('"%s" %s', $commandName, $this->command);
    }
}
