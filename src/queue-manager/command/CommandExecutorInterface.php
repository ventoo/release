<?php

namespace Phoenix\ReleaseUtil\QueueManager\Command;

interface CommandExecutorInterface
{
    public function execute(CommandInterface $command): void;
}
