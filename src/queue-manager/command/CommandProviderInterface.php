<?php

namespace Phoenix\ReleaseUtil\QueueManager\Command;

interface CommandProviderInterface
{
    public function getAll(): CommandCollection;
}
