<?php

namespace Phoenix\ReleaseUtil\QueueManager\Command;

use Phoenix\Utils\IdenticalInterface;

interface CommandInterface extends IdenticalInterface
{
    public function execute(): void;

    public function undo(): void;
}
