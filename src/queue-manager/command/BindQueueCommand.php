<?php

namespace Phoenix\ReleaseUtil\QueueManager\Command;

use Phoenix\QueueManager\Components\QueueData;
use Phoenix\Utils\IdenticalTrait;

class BindQueueCommand implements CommandInterface
{
    use IdenticalTrait;

    private $data;

    private $bindings;

    public function __construct(QueueData $data, array $bindings)
    {
        $this->data = $data;
        $this->bindings = $bindings;
    }

    public function execute(): void
    {
        $manager = $this->data->getManager();
        $queue = $this->data->getName();
        foreach ($this->bindings as $binding) {
            $manager->bindQueue($queue, $binding['routingKey'], $binding['exchange']);
        }
    }

    public function undo(): void
    {
        $manager = $this->data->getManager();
        $queue = $this->data->getName();
        foreach ($this->bindings as $binding) {
            $manager->unbindQueue($queue, $binding['routingKey'], $binding['exchange']);
        }
    }

    public function __toString()
    {
        return \sprintf('queue "%s" bindings %s', $this->data->getName(), \json_encode($this->bindings));
    }
}
