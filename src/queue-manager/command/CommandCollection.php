<?php

namespace Phoenix\ReleaseUtil\QueueManager\Command;

use Phoenix\Utils\AbstractList;

/**
 * @method bool add(CommandInterface $element)
 * @method bool contains(CommandInterface $element)
 * @method bool remove(CommandInterface $element)
 * @method CommandInterface getAt(int $index)
 * @method void addAt(int $index, CommandInterface $element)
 * @method CommandInterface setAt(int $index, CommandInterface $element)
 * @method CommandInterface removeAt(int $index)
 * @method int indexOf(CommandInterface $element)
 * @method int lastIndexOf(CommandInterface $element)
 * @method CommandInterface[] getIterator()
 * @method CommandInterface first() throws \UnderflowException when empty
 */
class CommandCollection extends AbstractList
{
    public function isValidElement($element): bool
    {
        return $element instanceof CommandInterface;
    }
}
